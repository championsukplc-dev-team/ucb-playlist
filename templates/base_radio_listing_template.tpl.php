<?php
/**
 *  theme to create a renderable playlist listing
 *
 * User: matty
 * Date: 20/12/2016
 * Time: 10:52
 * $vars
 *      date
 *      artist
 *      title
 *      itunes_link
 *      itunes_artwork
 *      itunes_preview
 *
 */
$nl = "\n";
$out = "" .$nl ;
$out .= "<div class=\"playlist-item\"  id=\"playlist-item-". $vars['id']."\">"  .$nl;
$out .= "    <div class=\"playlist-track\">"  .$nl;
$out .="            <div class=\"tracktime\">" . date('H:i', strtotime($vars['date'])) . "</div>"  .$nl;
$out .= "                <div class=\"trackinfo\"><div class=\"artist\">" . $vars['artist'] . "</div>"  .$nl;
$out .= "                <div class=\"tracktitle\">" . $vars['title'] . "</div>" .$nl;
$out.="            </div>" .$nl;
$out .= "          <div class=\"buy\">" .$nl;
if (!empty($vars['itunes_link'])) {
    $out .= "            <a href=\"" . $vars['itunes_link'] . "\" target=\"_blank\"><img alt=\"" . $vars['artist'] . " - " . $vars['title'] . "\" src=\"" . $vars['itunes_artwork'] . "\" /></a>" .$nl;
}else{
    switch($vars['station']){
      case 'gospel':
      case 'word':
      case 'uk':
        $out .= ucb_get_default_img('Radio Playlist Item - UCB 1');
      break;
      case 'inspirational':
        $out .= ucb_get_default_img('Radio Playlist Item - UCB 2');
      break;
    }
}
$out .="           </div>" .$nl;
$out .="    </div>" .$nl;
$out .="</div>" .$nl;
print $out;
