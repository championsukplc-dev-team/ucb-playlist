<?php
/**
 *  theme to create a renderable playlist lastest
 *  * $vars
 *      date
 *      artist
 *      title
 *      itunes_link
 *      itunes_artwork
 *      itunes_preview
 * User: matty
 * Date: 20/12/2016
 * Time: 10:52
 */
$station = isset($vars['station'])?$vars['station']:'';
$vars['station']= isset($vars['station'])?$vars['station']:'';
$vars['itunes_link']= isset($vars['itunes_link'])?$vars['itunes_link']:'';
$vars['itunes_artwork']= isset($vars['itunes_artwork'])?$vars['itunes_artwork']:'';
$vars['artist']= isset($vars['artist'])?$vars['artist']:'';
$vars['title'] = isset($vars['title'])?$vars['title']:null;
if ($vars['title']==null){ return "";}
$nl = "\n";
$out = "";
$out .= '<div class="playlist-item">' . $nl;
$out .= '    <div class="buy">' . $nl;
  if (!empty($vars['itunes_link'])) {
      $out .= "<a href='" . $vars['itunes_link'] . "'><img src='" . $vars['itunes_artwork'] . "' alt='" . $vars['artist'] . ' - ' . $vars['title'] . " ' /></a>" . $nl;
  } else {
    switch($vars['station']){
      case 'gospel':
      case 'word':
      case 'uk':
        $out .= ucb_get_default_img('Radio Playlist Item - UCB 1');
      break;
      case 'inspirational':
        $out .= ucb_get_default_img('Radio Playlist Item - UCB 2');
      break;
    }
  }
$out .= '    </div>' . $nl;/////buy
$out .= '    <div class="playlist-track">' . $nl;
//$out .= '        <div class="tracktime">' . $nl;
//$out .= '           ' . date('H:i', strtotime($vars['date'])) . $nl;
//$out .= '         </div>'; /// tracktime
$out .= '        <div class="tracktitle">' . $nl;
$out .= '           ' . $vars['title'] . $nl;
$out .= '        </div>';///tracktitle
$out .= '        <div class="trackinfo">' . $nl;
$out .= '            <div class="artist">' . $nl;
$out .= '             ' . $vars['artist'] . $nl;
$out .= '            </div>';///track info
$out .= '        </div>';///track info
$out .= '    </div>';///playlist track
$out .= '</div>' . $nl; ///playlist item
print $out;
