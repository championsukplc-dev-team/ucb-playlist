## UCB Playlist Module

 
##  Synopsis 

This Module handles the data collection side of the playlists handled by RCS ZETTA via a module custom page and stores to the custom table aw_stations, and also generates cached versions of the playlist.

The module will also work with the playlist javascript to pull the playlist back into the page as either pre rendered HTMl or as JSON data


## Theme templates
this module includes two templates. 
* The first is for rendering a single "latest" block of html (`sites/all/modules/ucb/ucb_playlist/templates/base_radio_latest_template.tpl.php`). 
* The second is a template for each row in a play list (`sites/all/modules/ucb/ucb_playlist/templates/base_radio_listing_template.tpl.php`)

## DATABASE TABLE aw_station schema
using Drupal's .install table, one table is set up as part of the module install.

`aw_station`
      
      'id' = auto id int() 
      'date' = datetime 
      'station' = varchar 50
      'artist' = varchar 255
      'title' = varchar 255
      'event_type' = varchar 25
      'itunes_link' = varchar 255 
      'itunes_artwork' = varchar 255 
      'itunes_preview' = varchar 255

    

## Example urls for calling the playlist data 

/listencache/[type]/[station]/[day]/(optional)[format] 
* type - can be latest or playlist. Latest shows the last show added for the station for day 0. Playlist will show the latest shows for a station for a day
    * latest
    * playlist
    * combined - returns json encoded array
* station - 

    * gospel =Gosple
    * inspirational = Inspirational
    * uk = UCB UK
    * word = UCB Word
* day
    * 0
    * 1
    * 2
    * 3
* format
    * html - default
    * json
    

### URLS to call
 
`Rendered HTML`

 * /listencache/playlist/uk/0
 * /listencache/latest/uk/0
 * listencache/playlist/word/1
 
 `Rendered JSON`
 
 * /listencache/playlist/uk/0/json
 * /listencache/latest/uk/0/json
 * listencache/playlist/word/1/json
 
## Combined Cache URL 
 `Combined`
 * /listencache/`combined`/uk/0
 * /listencache/`combined`/uk/1 -> might not be required as you always update the day one playlist, if that's the case always send a 0  for your day in the url
 
 
#### Combined Return JSON
 
 * `latestHTML`           : HTML of the latest song
 * `latestListingHTML`    : HTML of the last listings entry
 * `latestListingID`      : Id of the last entry in the listing|null,
 
  JSON
 ```
 {
 "latestHTML": "\n\n<!-- THEME DEBUG -->\n<!-- CALL: theme('base_radio_latest_template') -->\n<!-- BEGIN OUTPUT from 'sites/all/modules/ucb/ucb_playlist/templates/base_radio_latest_template.tpl.php' -->\n<div class=\"playlist-item\">\n    <div class=\"buy\">\n<a href='https://itunes.apple.com/gb/album/the-christmas-song/id299613166?i=299613192&uo=4'><img src='http://is3.mzstatic.com/image/thumb/Music/v4/87/a0/30/87a03008-27ff-4f15-cb76-24bda08d8bf7/source/60x60bb.jpg' alt='GARY CHAPMAN - THE CHRISTMAS SONG ' /></a>\n    </div>\n    <div class=\"playlist-track\">\n        <div class=\"tracktitle\">\n           THE CHRISTMAS SONG\n        </div>        <div class=\"trackinfo\">\n            <div class=\"artist\">\n             GARY CHAPMAN\n            </div>        </div>    </div></div>\n\n<!-- END OUTPUT from 'sites/all/modules/ucb/ucb_playlist/templates/base_radio_latest_template.tpl.php' -->\n\n",
 "latestListingHTML": "\n\n<!-- THEME DEBUG -->\n<!-- CALL: theme('base_radio_listing_template') -->\n<!-- BEGIN OUTPUT from 'sites/all/modules/ucb/ucb_playlist/templates/base_radio_listing_template.tpl.php' -->\n\n<div class=\"playlist-item\"  id=\"playlist-item-715\">\n    <div class=\"playlist-track\">\n            <div class=\"tracktime\">16:02</div>\n                <div class=\"trackinfo\"><div class=\"artist\">GARY CHAPMAN</div>\n                <div class=\"tracktitle\">THE CHRISTMAS SONG</div>\n            </div>\n          <div class=\"buy\">\n            <a href=\"https://itunes.apple.com/gb/album/the-christmas-song/id299613166?i=299613192&uo=4\" target=\"_blank\"><img alt=\"GARY CHAPMAN - THE CHRISTMAS SONG\" src=\"http://is3.mzstatic.com/image/thumb/Music/v4/87/a0/30/87a03008-27ff-4f15-cb76-24bda08d8bf7/source/60x60bb.jpg\" /></a>\n           </div>\n    </div>\n</div>\n\n<!-- END OUTPUT from 'sites/all/modules/ucb/ucb_playlist/templates/base_radio_listing_template.tpl.php' -->\n\n",
 "latestListingID": "715"
 }
 ```
## Adding Data from ZETTA or adding data with the admin users
 
 Both ZETTA and the admin user will be posting data to:
    [siteurl]/import_glu_playlist
    
    with the expected post variables of 
    
    *  $_POST['station'] = "ucbuk";
    *  $_POST['start_time'] = "2016-12-16 13:10:02";
    *  $_POST['artist'] = "Ocean Colour Scene";
    *  $_POST['title'] = "so low";
    *  $_POST['event_type'] = "song";
    
 however, logged into the `admin user`, we can post our own variables into the data during testing
 
 simply add [siteurl]/import_glu_playlist`?form` to the end of the url
 
 You should now be able to choose the station, enter the start time/artist/title and event_type
## Calling the cached data from php code
 

   Build the cache name for a station/day/html or json and if requested, return the cached data itself
  
  * @param string $output - either output the `cid`/`cache` name or the cached data itself
  * @param string $station -  `see the key names from aw_playlist_stationsSettings()`
  * @param int $day for playlist we can use `0`,`1`,`2`,`3` as the days, latest is forced to be set to `0` 0 is today, 1 is yesterday and so on
  * @param string $type - can be `playlist` or `latest`
  * @param bool $json - if set to `true` the cid name will be altered to return the array (I called it json as in another use, a converted the array into a json string), otherwise `false` we return the prebuilt html
  * @return bool|string|array returns false or a string of output
  
  
  
 print  `aw_playlist_CacheCids`("`cache`", $station = "`uk`", $day=`0`, $type="`playlist`", $json = `false`); ///prints rendered html
 
 
 
 