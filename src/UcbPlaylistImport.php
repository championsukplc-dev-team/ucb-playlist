<?php

namespace UcbPlaylist;

/**
 * CLASS to capture the requests from RCS Zetta and turn them into inserts in the aw_station table, or to ignore capture and rebuild the caches for a give station
 *  IF DATA CAPTURE
 *      - capture fields
 *      - qualify captured data
 *      - query itunes api to get url/mp3 preview and album image
 *  IF DATA CAPTURE OR JUST A CACHE BUILD REQUEST
 *      - creates the cached pages for the radio station for
 *          latest
 *          day 0 (today)
 *          day 1
 *          day 2
 *          day 3
 *
 */
class UcbPlaylistImport
{
    public $outString;
    /**
     * $station
     * @var the station we'll be storing the data against
     */
    public $station;
    public $debugData = array();
    /**
     * @var $event_type one of the following    link | song | spot
     */
    private $event_type;
    /**
     * @var string $title
     */
    private $title;
    /**
     * @var string $artist
     */
    private $artist;
    /**
     * @var string $start_date format 2012-12-29 19:06:15
     */
    private $start_time;
    /**
     * @var string $itunes_link
     */
    private $itunes_link;
    /**
     * @var string $itunes_preview
     */
    private $itunes_preview;
    /**
     * @var string $itunes_artwork
     */
    private $itunes_artwork;
    /**
     * @var bool flag to see how things are progressing
     */
    private $okToProceed;
    private $debug;
    /* @var $checkDate will be used to work out if this post has been posted in the last minute */
    private $checkDate;

    /**
     * ImportRCSZettaPosts constructor.
     */
    public function __construct($captureGet = true, $stationOverride = false, $debug = false)
    {

        $this->outString = "";
        $this->debug = $debug;
        if ($this->debug == true) {
            $this->debugData['stationData'] = array(
                0 => array(),
                1 => array(),
                2 => array(),
                3 => array(),
            );
        }

        /* when captureGet  == true it means that we are using the class to insert a new record , if false, we are using the class to just rebuild the cache */
        if ($captureGet == true) {
            $this->okToProceed = true;
            //////////////debug data coming in
            if (variable_get('RCSZettaLogPosts', 'no') == 'yes') {
                $postVariables = (array) $_POST;
                watchdog("zettapost", "<PRE>" . print_r($postVariables, true) . "</pre>");
            }
            //////////////debug data coming in - ENDS - remove after we figure what the hell is going on
            /*
             * expected variables example
             *  $_POST['station'] = "ucbuk";
             *  $_POST['start_time'] = "2016-12-16 13:10:02";
             *  $_POST['artist'] = "Ocean Colour Scene";
             *  $_POST['title'] = "so low";
             *  $_POST['event_type'] = "song";
             *
             */
            if (!isset($_POST['station']) || !isset($_POST['start_time']) || !isset($_POST['event_type'])) {
                die("MISSING A POST VARIABLE");
            }
            /* set the itunes defaults*/
            $this->itunes_artwork = "";
            $this->itunes_link = "";
            $this->itunes_preview = "";
            $this->extractPostData($this->postVariablesToRip());
            if ($this->okToProceed == true) {
                $this->processPost(); ////convert and insert into sql
            }
        } else if ($stationOverride != "") {
            $this->station = $stationOverride;
        }
        if ($stationOverride != false && $stationOverride != "") {
            $this->station = $stationOverride;
        }
        $this->buildCaches();
    }

    /**
     * this will turn the variables from the post into class variables, then check the variables are set
     * @param $post_varibles
     */
    private function extractPostData($post_varibles)
    {
        $variables_set = 0;

        foreach ($post_varibles as $post_variable_name => $variable_data) {
            //cycle through each post setting
            if (isset($_POST[$post_variable_name])) { //if present, look further into sanatising the post value
                if ($_POST[$post_variable_name] != "") {
                    $capture = $this->safe_txt($_POST[$post_variable_name]); /// sanatize
                    switch ($variable_data['type']) {
                        /* if it's a string then we need to shorten the value to the prescribe length */
                        case "string":
                            $len = isset($variable_data['size']) ? $variable_data['size'] : 0;
                            $capture = substr($capture, 0, $len);
                            break;
                        case "date": ///do some date checks

                            if (strlen($capture) > 8) {
                                ////this was in the old code, must be here for a reason
                                #$this->$post_variable_name = $capture;

                            } else {

                                $capture = date("Y-m-d", time()) . " " . $capture;

                            }
                            #die(strlen($capture)  . " ". $this->$post_variable_name );
                            break;

                    }
                    $this->$post_variable_name = $capture;
                }
            } else {
                $this->$post_variable_name = false; ////if a bad match set the class variable to false
            }
        }

        foreach ($post_varibles as $post_variable_name => $variable_data) {
            switch ($post_variable_name) {
                case "station":
                    $this->station = $this->getStationFromPOST($this->station);
                    if ($this->station == false) {
                        die("ERROR NO STATION MATCH");
                    }

                    break;
            }
            if ($this->$post_variable_name == false) {
                $this->okToProceed = false;
            }
            # echo "$post_variable_name:: ". $this->$post_variable_name ." = " . print_r($variable_data,true) ."<BR>";
        }
    }

    /**
     * @param $in string
     * @return string
     */
    private function safe_txt($in = "")
    {
        return preg_replace("/\n|\r/", '', filter_var($in, FILTER_SANITIZE_STRING));
    }

    /***************************************
     * swap station to table names
     * examples -> ucbuk, gospel, insp, word
     *
     * todo: we might need to make this into a forms settings set of fields rather than a hard coded list
     *************************************/
    public function getStationFromPOST($station = "")
    {

        switch ($station) {
            case 'insp':
            case 'INSP':
            case 'ucbinsp':
            case 'UCBINSP':
                return 'inspirational';
                break;
            case 'ucbuk':
            case 'UCBUK':
                return 'uk';
                break;
            case 'gospel':
            case 'GOSPEL':
                return 'gospel';
                break;
            case 'word':
            case 'WORD':
                return 'word';
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * @return array of fields we expect to be posted to the page
     */
    public function postVariablesToRip()
    {
        $post_variables = [
            'station' => ['size' => 50, 'type' => 'string'],
            'event_type' => ['size' => 50, 'type' => 'string'],
            'title' => ['size' => 255, 'type' => 'string'],
            'artist' => ['size' => 50, 'type' => 'string'],
            'start_time' => ['size' => 0, 'type' => 'date'],
        ];
        return $post_variables;
    }

    /**
     * take the santioned values so far and check the event type to process itunes data and then save the complete set
     */
    private function processPost()
    {
        /* if event type song then get itunes data*/
        $this->event_type = strtolower($this->event_type); ///////////RTC ZETA are sending the data over as Song, not song, so adding a case conversion to the value
        switch ($this->event_type) {
            case "song":
                $this->getItunesData();
                break;
        }
        /* insert into sql*/
        $this->InsertToDB();

    }

    /**
     * using Itunes json lookup to try and pull a url/preview and image
     */
    private function getItunesData()
    {
        $url = 'http://itunes.apple.com/search?term=' .
        str_replace(' ', '+', (string) ucwords($this->artist)) .
        '+' .
        str_replace(' ', '+', (string) ucwords($this->title)) .
            '&country=GB&media=music&limit=6&genreId=&version=2&output=json';
        $objItunes = json_decode(utf8_encode(file_get_contents($url, 0, null, null)));
        if (!isset($objItunes->results[0])) {
            $kind = isset($objItunes->results[0]->kind) ? $objItunes->results[0]->kind : 'na';
            if ($kind != 'song') {
                $objItunes = null;
            } else {

            }
            $objItunes = null;
        }

        /* if a result has been found and it's a song  */
        if (isset($objItunes->results[0]->trackViewUrl) && isset($objItunes->results[0]->artworkUrl60) && isset($objItunes->results[0]->previewUrl)) {
            $this->itunes_link = $objItunes->results[0]->trackViewUrl;
            $this->itunes_artwork = $objItunes->results[0]->artworkUrl60;
            $this->itunes_preview = $objItunes->results[0]->previewUrl;
            /*make values db ready*/
            $this->itunes_link = substr($this->itunes_link, 0, 255);
            $this->itunes_artwork = substr($this->itunes_artwork, 0, 255);
            $this->itunes_preview = substr($this->itunes_preview, 0, 255);
        }

        $objItunes = null;

    }

    /**
     * mysql record insert
     */
    private function InsertToDB()
    {
        #$countSQLQuery = "SELECT COUNT(*) FROM aw_station   WHERE    station='%s'  AND  date='%s' AND  artist= '%s' AND  title ='%s' AND  event_type = '%s' ";
        #$sql = sprintf($countSQLQuery, $this->station, $this->start_time, $this->artist, $this->title, $this->event_type);
        /* zeta are doing double posts for some reason, this aims to ignore these duplicate posts by seeing if there's any other posts like this one posted within the last minute */
        /* we will need to  check to see if this entry has been posted in the last minute during sql insert , create the value required below*/
        $ctime = strtotime($this->start_time) - (60 * 1); //// turn time into unix time  - 60 seconds
        $this->checkDate = date('Y-m-d H:i:s', $ctime); ///convert the date back into sql friendly date
        $countSQLQuery = "SELECT COUNT(*) FROM aw_station   WHERE    station='%s'  AND  date >='%s' AND  artist= '%s' AND  title ='%s' AND  event_type = '%s' ";
        $sql = sprintf($countSQLQuery, $this->station, $this->checkDate, $this->artist, $this->title, $this->event_type);
        $count = db_query($sql)->fetchField();
        if ($count == 0) {
            $sql = "INSERT  IGNORE INTO aw_station
              ( station, date, artist, title, event_type, itunes_link, itunes_artwork, itunes_preview)
               VALUES
              ('%s','%s','%s','%s','%s','%s','%s','%s')
           ";

            $sql = sprintf($sql, $this->station, $this->start_time, $this->artist, $this->title, $this->event_type, $this->itunes_link, $this->itunes_artwork, $this->itunes_preview);
            if ($this->debug == true) {
                $this->debugData['insert'] = $sql;
            }

            try {
                db_query($sql);
            } catch (Exception $e) {
                krumo($e);
            }
        }
    }

    /**
     * set up the days to build and build each day
     */
    public function buildCaches()
    {
        if ($this->station == null or $this->station == false) {
            #echo "NO STATION SELECTED";
        } else {
            $days = [3, 2, 1, 0];
            foreach ($days as $day) {
                $this->buildDateCache($day);
            }
        }
    }

    /* */

    /**
     * for the days we need to build a listing in the cache. IF Day 0, then we also need to build a latest section
     * @param int $day
     */
    private function buildDateCache($day = 0)
    {
        $results = ucb_radio_station_bygones($day, $this->station);
        if ($this->debug == true) {
            $this->debugData['stationData'][$day]['results'] = $results;
        }
        #echo "$day {$this->station}";
        if ($day == 0) {
            $this->buildDataCacheDay0Latest($results, $this->station, $day); ///build the laetest cache
        }
        $this->buildDataCacheDay0List($results, $this->station, $day); ///build the listing cache
    }

    /**
     * build the latest drupal cache for a station
     * get the latest song from your array and build the latest HTML and save it to the drupal cache
     * @param $results
     * @param $station
     * @param $day
     */
    private function buildDataCacheDay0Latest($results, $station, $day)
    {
        $cache_save_name = "playlistlatest_" . $station . "_" . $day;
        $array = array();

        $results[0]['station'] = $station;

        $out = \Drupal::service('renderer')->render([
            '#theme' => 'base_radio_latest_template',
            '#items' => $results[0]
        ]);

        if (!empty($results[0]['itunes_link'])) {
            $out .= "<a href='" . $results[0]['itunes_link'] . "'><img src='" . $results[0]['itunes_artwork'] . "' alt='" . $results[0]['artist'] . ' - ' . $results[0]['title'] . " ' /></a>";
        } else {
            $results[0]['itunes_link'] = "";
            $results[0]['itunes_artwork'] = "";
            $results[0]['itunes_preview'] = "";
        }
        /*build array version*/
        $array = [
            'station' => $station,
            'id' => isset($results[0]['id']) ? $results[0]['id'] : null,
            'date' => $results[0]['date'],
            'tracktime' => date('H:i', strtotime($results[0]['date'])),
            'artist' => $results[0]['artist'],
            'title' => $results[0]['title'],
            'itunes_link' => $results[0]['itunes_link'],
            'itunes_artwork' => $results[0]['itunes_artwork'],
            'itunes_preview' => $results[0]['itunes_preview']
        ];

        $this->outString = $out;

        /*
         * todo: when the palyer pages get the content via ajax, aren't we putting a big load onto the server by serving it up via the drupal cache?
         * */
        cache_set($cache_save_name, $out, 'cache'); ///simply save to the cache default fourth value is set to defined variable CACHE_PERMANENT
        cache_set("json" . $cache_save_name, $array, 'cache'); ///simply save to the cache default fourth value is set to defined variable CACHE_PERMANENT

        if ($this->debug == true) {
            $this->debugData['latest'] = ['html' => $out, 'json' => $array];
        }
    }

    /**
     * builds a list of results and caches
     * @param $results
     * @param $station
     * @param $day
     */
    private function buildDataCacheDay0List($results, $station, $day)
    {
        $cache_save_name = "playlistcache_" . $station . "_" . $day;
        $out = "";
        $arrays = array();
        foreach ($results as $result) {
            $array = array();

            $result['station'] = $station;
            $out .= theme('base_radio_listing_template', array('vars' => $result));
            $array['id'] = $result['id'];
            $array['date'] = $result['date'];
            $array['tracktime'] = date('H:i', strtotime($result['date']));
            $array['artist'] = $result['artist'];
            $array['title'] = $result['title'];
            $array['itunes_link'] = $result['itunes_link'];
            $array['itunes_artwork'] = $result['itunes_artwork'];
            $array['itunes_preview'] = $result['itunes_preview'];
            $array['station'] = $station;
            $arrays[] = $array;
        }

        cache_set($cache_save_name, $out, 'cache'); ///simply save to the cache  default fourth  value is set to defined variable CACHE_PERMANENT
        cache_set("json" . $cache_save_name, $arrays, 'cache'); ///simply save to the cache  default fourth  value is set to defined variable CACHE_PERMANENT
        $this->outString = $out;
        if ($this->debug == true) {
            $this->debugData['stationData'][$day]['html'] = $out;
            $this->debugData['stationData'][$day]['json'] = $arrays;

            $this->debugData['stationData'][$day]['html_cache_name'] = $cache_save_name;
            $this->debugData['stationData'][$day]['json_cache_name'] = "json" . $cache_save_name;
        }
    }
}
