<?php

/**
 * @file
 * Contains Drupal\ucb_playlist\Form\MessagesForm.
 */

namespace Drupal\ucb_playlist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ucb_playlist\UcbPlaylistCache;
use Drupal\ucb_playlist\UcbPlaylistStation;
use Drupal\ucb_playlist\UcbPlaylistHelper;

class AdminLanding extends ConfigFormBase
{
    const LANDING_URL = 'admin/content/playlists';

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return ['ucb_playlist.adminsettings'];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'ucb_playlist_admin_landing';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, $form_state, $selected_station = null, $selected_day = null)
    {
        $form['ucb_playlist_admin_landing_markup'] = [
            '#markup' => $this->menuBuild($selected_station, $selected_day) . $this->pageBuild($selected_station, $selected_day),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * builds a basic menu from the stations and days
     *
     * @param string $selected_station
     * @param string $selected_day
     * @return string
     */
    public function menuBuild($selected_station, $selected_day)
    {
        if (\Drupal::request()->query->get('rowID') && is_numeric(\Drupal::request()->query->get('rowID'))) {
            die(ucb_playlist_admin_landingRemove(\Drupal::request()->query->get('rowID')));
        }

        if (!is_null(\Drupal::request()->query->get('clear')) || $selected_station == 'clear') {
            UcbPlaylistCache::clear($selected_station);
        }

        $days = $stations = [];
        foreach (UcbPlaylistStation::list() as $key => $station) {
            $stations[] = Link::fromTextAndUrl($station['name'], Url::fromRoute('ucb_playlist.admin.index', ['selected_station' => $key, 'selected_day' => $selected_day], ['attributes' => ['class' => [($selected_station == $key) ? 'is-active' : '']]]))->toString();
        }

        foreach (["Today", "Yesterday", "Two Days Ago", "Three Days Ago"] as $key => $day) {
            $days[] = Link::fromTextAndUrl($day, Url::fromRoute('ucb_playlist.admin.index', ['selected_station' => $selected_station, 'selected_day' => $key], ['attributes' => ['class' => [($selected_day == $key) ? 'is-active' : '']]]))->toString();
        }

        $str = "<a class='button button-action button--primary button--small' href='/" . self::LANDING_URL . "?station=$selected_station&clear'>Clear Cache</a>";
        $str .= "<div class='panel'>";
        $str .= "<p>Stations:: " . implode(' | ', $stations) . "</p>";
        $str .= "<p>Days:: " . implode(' | ', $days) . "</p>";
        $str .= "</div>";

        return $str;
    }

    /**
     * Builds the admin listing
     * @param string $selected_station
     * @param string $selected_day
     * @return string
     */
    public function pageBuild($selected_station, $selected_day)
    {
        $entries = UcbPlaylistHelper::bygones(round($selected_day), $selected_station);

        $rows = "";
        foreach ($entries as $row) {
            $img = isset($row['itunes_artwork']) ? $row['itunes_artwork'] : '';
            if ($img != '') {
                $img = "<img src='$img'>'";
            }

            $rows .= "<tr id='ROW" . $row['id'] . "'>";
            $rows .= "<td>" . $row['date'] . "</td>";
            $rows .= "<td>" . $row['event_type'] . "</td>";
            $rows .= "<td>" . $row['station'] . "</td>";
            $rows .= "<td>" . $row['artist'] . "</td>";
            $rows .= "<td>" . $row['title'] . "</td>";
            $rows .= "<td>" . $img . "</td>";
            $rows .= "<td><button id='" . $row['id'] . "' data-id='" . $row['id'] . "' data-clicks='no' class='removeentry'>Remove</button></td>";
            $rows .= "</tr>";
        }
        if ($rows != "") {
            $rows = "<table class='responsive-enabled sticky-enabled sticky-table'>
                <tr>
                   <th><b>date</b></th>
                   <th><b>type</b></th>
                   <th><b>station</b></th>
                   <th><b>artist</b></th>
                   <th><b>title</b></th>
                   <th><b>img</b></th>
                   <th width='100'><b>Remove</b></th>
                </tr>
                $rows</table>";

            $js = "
            jQuery('.removeentry').click(function(){
                console.log(this);
                var rowID = jQuery(this).data('id');
                var clicks = jQuery(this).data('clicks');
                if (clicks == 'no')
                {
                    jQuery(this).html('Are you sure you want to remove?');
                    jQuery(this).data('clicks', 'yes');
                    console.log(jQuery(this).data('clicks'));
                }else
                {
                    jQuery(this).html('Removing');
                    var theURL = '/" . Url::fromRoute('ucb_playlist.admin.index', ['selected_station' => $selected_station, 'selected_day' => $selected_day])->toString() . "/'+ rowID ;

                    jQuery.ajax({

                       type:'GET',
                       url:theURL,
                       cache:false,
                       success: function(response) {
                           jQuery('#ROW'+rowID ).remove();
                           }
                    });
                }
            });
        ";
            $rows .= '<script>jQuery(document).ready(function () { ' . $js . ' });</script>';
        } else {
            $rows = " <strong>No Entries for Today</strong>";
        }
        return $rows;
    }

}
