<?php

/**
 * @file
 * Contains Drupal\ucb_playlist\Form\ImportGlu.
 */

namespace Drupal\ucb_playlist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ucb_playlist\UcbPlaylistCache;
use Drupal\ucb_playlist\UcbPlaylistStation;
use Drupal\ucb_playlist\UcbPlaylistHelper;

class ImportGlu extends ConfigFormBase
{
    const LANDING_URL = 'admin/content/playlists';

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return ['ucb_playlist.adminsettings'];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'ucb_playlist_admin_landing';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, $form_state, $selected_station = null, $selected_day = null)
    {
        $stations = ['ucbuk' => 'uk', 'insp' => 'inspirational', 'gospel' => 'gospel', 'word' => 'word'];

        $form['station'] = ['#type' => 'select', '#options' => $stations, '#default_value' => 'uk'];
        $form['start_time'] = ['#type' => 'textfield', '#title' => 'Start Time', '#default_value' => date('Y-m-d H:i:s', time())];
        $form['artist'] = ['#type' => 'textfield', '#default_value' => 'Ocean Colour Scene'];
        $form['title'] = ['#type' => 'textfield', '#default_value' => 'Day we caught the train'];
        $form['event_type'] = ['#type' => 'textfield', '#title' => 'Event Type', '#default_value' => 'song'];

        return parent::buildForm($form, $form_state);
    }
}
