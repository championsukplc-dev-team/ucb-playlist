<?php

/**
 * @file
 * Contains Drupal\ucb_playlist\Form\MessagesForm.
 */

namespace Drupal\ucb_playlist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ucb_playlist\UcbPlaylistCache;
use Drupal\ucb_playlist\UcbPlaylistStation;
use Drupal\ucb_playlist\UcbPlaylistHelper;

class Cache extends ConfigFormBase
{
    const LANDING_URL = 'admin/content/playlists';

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return ['ucb_playlist.adminsettings'];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'ucb_playlist_admin_landing';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, $form_state)
    {
        UcbPlaylistCache::clear();
    }
}
