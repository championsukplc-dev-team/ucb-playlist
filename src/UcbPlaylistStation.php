<?php

namespace Drupal\ucb_playlist;

class UcbPlaylistStation
{
    public static function list()
    {
        return [
            'uk' => ['name' => 'UCB UK'],
            'inspirational' => ['name' => 'Inspirational'],
            'gospel' => ['name' => 'Gospel'],
            'word' => ['name' => 'UCB Word']
        ];
    }
}
