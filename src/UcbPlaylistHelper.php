<?php

namespace Drupal\ucb_playlist;

class UcbPlaylistHelper
{
    /**
     * run a sql query to build a playlist for a day for a station
     * @param int $day + X days
     * @param string $stationcode represents the columns in the aw_station station field
     * @return array of results from mysql lookup
     */
    public static function bygones($day = 0, $stationcode = "uk")
    {
        $db = \Drupal::database();

        if ($day > 0) {
            $dateTime = new \DateTime("now -{$day} day");
            $from = $dateTime->setTime(00, 00, 00)->format('Y-m-d H:i:s');
            $to = $dateTime->setTime(23, 59, 59)->format('Y-m-d H:i:s');
        } else {
            $dateTime = new \DateTime();
            $to = $dateTime->format('Y-m-d H:i:s');
            $from = $dateTime->setTime(00, 00, 00)->format('Y-m-d H:i:s');
        }

        $todaysResults = $db->query("SELECT * FROM aw_station g WHERE g.station ='$stationcode' AND  g.date BETWEEN '$from' AND '$to' AND artist<>'' ORDER BY g.date DESC;");

        $arrReturn = array();
        foreach ($todaysResults as $result) {
            $result['title'] = (strlen($result['title']) > 39) ? substr($result['title'], 0, 36) . '...' : $result['title'];
            $arrReturn[] = $result;
        }
        return $arrReturn;
    }
}
