<?php

namespace Drupal\ucb_playlist;

class UcbPlaylistCache
{

    /**
     * clear
     *
     * @return void
     */
    public static function clear()
    {
        \Drupal::cache("playlist_latest")->invalidateAll();
        \Drupal::cache("json_playlist_latest")->invalidateAll();
        \Drupal::messenger()->addMessage('Cache cleared for playlists');
    }
}
