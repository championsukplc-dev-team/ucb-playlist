<?php
/**
 * Created by PhpStorm.
 * User: matty
 * Date: 15/12/2016
 * Time: 12:41
 */


function aw_playlist_settingsform($form, &$form_state)
{

    $form['#attributes']['enctype'] = 'multipart/form-data'; // If this is not here, upload will fail on submit


    $form['RCSZetta'] = array(
        '#type' => 'fieldset',
        '#title' => t('RCS ZETTA'),
        '#weight' => 5,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    /*
     * todo: potentially no need for file cache if we use drupal caches
     * */
    /* this variable will be used during the caching and getting of data */
    $form['RCSZetta']['RCSZettaWriteFolder'] = array(
        '#type' => 'textfield',
        '#title' => t('RSC Zetta - Cached folder'),
        '#description' => t('Please name the folder within your siteroot folder\'s assets that you\'d like the cache files written to'),
        '#prefix' => '/<SITEROOT>/assets/',
        '#default_value' => variable_get('RCSZettaWriteFolder', 'playlistcache'), ////// folder will be   something like playlistcache > making it /var/www/assets/playlistcache
    );

    return system_settings_form($form); #save to varible store
}
