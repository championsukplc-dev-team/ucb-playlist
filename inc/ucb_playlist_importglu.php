<?php

/**
 * used to capture posts from RSC Zetta
 * URL: import_glu_playlist
 * capture the pushes from RCS Zetta and record in the db
 * build cache
 *
 * uses class ImportRCSZettaPosts
 *
 * if you use the url import_glu_playlist?form and logged in as admin user, you can post your own songs to the system via a form
 *
 */

function import_glu_ucb_playlist()
{
    $debug = false;
    global $user;
    $postCount = 0;
    if (isset($_POST)) {
        $postCount = count($_POST);
    }
    if ($user->uid == 1 and isset($_GET['form'])) {
        $debug = true;

        if ($postCount == 0) {
            $form = drupal_get_form("import_glu_ucb_playlist_form");
            $str = drupal_render($form);
            return $str;
        }
    }
    $runBuild = true;
    require_once("ImportRCSZettaPosts.php");
    #$import = new ucbPlaylist\ImportRCSZettaPosts();
    $import = new ucbPlaylist\ImportRCSZettaPosts($runBuild, false, $debug);
    if ($debug == true) {
        if (function_exists("krumo")) {
            krumo($import);
        }
        if (isset($import->debugData['stationData'])) {
            foreach ($import->debugData['stationData'] as $day => $dayData) {
                echo "<h2>DAY + $day days </h2><fieldset>";
                if (isset($dayData['html'])) {
                    echo $dayData['html'];
                }
                echo "</fieldset>";
            }
        }
    }
}


/**
 *
 * testing form
 * @param $form
 * @param $formstate
 * @return mixed
 */
function import_glu_ucb_playlist_form($form, &$formstate)
{
    $stations = array(
        'ucbuk' => 'uk',
        'insp' => 'inspirational',
        'gospel' => 'gospel',
        'word' => 'word',
    );
    $form['station'] = array('#type' => 'select', '#options' => $stations, '#default_value' => 'uk');
    $phpdate = date('H:i:s', time());
    $today = date('Y-m-d', time());
    $to = date('Y-m-d H:i:s', strtotime("$today $phpdate"));
    $form['start_time'] = array('#type' => 'textfield', '#title' => 'Start Time', '#default_value' => '' . $to);
    $form['artist'] = array('#type' => 'textfield', '#default_value' => 'Ocean Colour Scene');
    $form['title'] = array('#type' => 'textfield', '#default_value' => 'Day we caught the train');
    $form['event_type'] = array('#type' => 'textfield', '#title' => 'Event Type', '#default_value' => 'song');
    $form['submit'] = array('#type' => 'submit', '#value' => 'save');
    return $form;
}
