<?php

/**
 * file called from the ajax part of the playlist
 */
/**
 *
 * drupal cache method of returning cached output
 *  url breakdown listencache/<type>/<station>/<day>
 * /listencache/playlist/uk/0
 * /listencache/latest/uk/0
 * listencache/playlist/word/1
 *
 */
function ucb_playlist_returncache()
{
    $args = arg();
    $displayType = isset($args[1])?$args[1]:null;
    $station = isset($args[2])?$args[2]:null;
    $day= isset($args[3])?$args[3]:null;
    $outputtype = isset($args[4])?$args[4]:"html"; ////json


    /// listencache
    if (aw_playlist_veryifyStation($station)==false){
        die(t("Sorry your station was not recognised the information you wanted could not be returned"));
    }
    switch ($displayType)
    {
        case "playlist":
            #echo "$station | $day ";
            ucb_playlist_returncache_PlayList($station, $day, 0, $outputtype );
            break;
        case "latest":
            ucb_playlist_returncache_Latest($station, $day, 0, $outputtype );
            break;
        case "combined":
            ucb_playlist_returncache_Combined($station, $day, 0  );
            break;
    }
}

/*
 * todo: get the latest station HTML, no matter what
 * todo: build cache if required
 * todo: if day ==0 then return the latest radio using the theme listing
 *
 * */
function  ucb_playlist_returncache_Combined($station="", $day=0, $tries = 0 ){

    $return = [
        'latestHTML' => "",
        'latestListingHTML'=> "",
    'latestListingID' => null,
    ];

    $return['latestHTML']= aw_playlist_CacheCids("cache", $station , 0, "latest", false, 0);
    $return['latestHTML'] = preg_replace('/<!--(.*)-->/Uis', '', $return['latestHTML']);
    if ($day ==0)
    {
        $type = "playlist";
        $json = true;
        $data = aw_playlist_CacheCids("cache", $station, $day, $type, $json);
        if (isset($data[0]))
        {
            $id =  isset($data[0]['id'])?$data[0]['id']:0;
            $data[0]['refreshed']=true;
            $rowStr = theme('base_radio_listing_template', array('vars'=>$data[0]));
            $rowStr= preg_replace('/<!--(.*)-->/Uis', '', $rowStr);
           # $rowStr ="<div class='updated-".time() ."'>'". $rowStr ."</div>";
            $return['latestListingID']= $id;
            $return['latestListingHTML']= $rowStr;

        }
    }

    drupal_json_output ($return);




}


/**
 * tells ucb_playlist_GetDisplay to return a playlist cache
 * @param string $station e.g.uk
 * @param int $day 0|1|2|3
 * @param int $tries 0 - to stop getting in a endless loop if the cache isnn't built
 */
function ucb_playlist_returncache_PlayList($station="", $day=0, $tries = 0, $outputtype ="html")
{
    $cid = "playlistcache_" . $station . "_" . $day;
    ucb_playlist_GetDisplay($station, $day, $tries,"playlist", $cid, $outputtype );
}

/**
 * tells ucb_playlist_GetDisplay to return a latest cache
 * @param string $station e.g. uk
 * @param int $day 0|1|2|3
 * @param int $tries 0 - to stop getting in a endless loop if the cache isnn't built
 */
function ucb_playlist_returncache_Latest($station="", $day=0, $tries =0 , $outputtype ="html")
{
    $cid = "playlistlatest_" . $station . "_" . $day;
    ucb_playlist_GetDisplay($station, $day, $tries,"playlist", $cid, $outputtype);
}

/**
 * Universal function used by both  ucb_playlist_returncache_Latest and  ucb_playlist_returncache_PlayList to cut down on code replication to get the items from the cache and to rebuild the cache if required
 * @param string $station
 * @param int $day 0|1|2|3
 * @param int $tries - to stop getting in a endless loop if the cache isnn't built
 * @param string $type playlist OR latest
 * @param string $cid the cache cid to pull
 */
function ucb_playlist_GetDisplay($station="", $day=0, $tries = 0, $type="playlist", $cid="", $outputtype ="html")
{
    if ($tries >1)
    {
        #echo " CACHE BUILDING ISSUES - COUNT: $tries";
      die("ERROR:901122");
    }
    $cstart="";
    if ($outputtype != "html")
    {
      $cstart="json";
    }

    $cached = cache_get($cstart.$cid);
    if( $cached  ==false)
    {
        /* if the cache doesn't exist because of a flush or the like, rebuild it and try again*/
        ucb_playlist_displayRebuildCache($station); ///triggers the class ImportRCSZettaPosts
        $tries ++; ///don't want to get stuck in a loop
        ucb_playlist_GetDisplay($station, $day, $tries, $type, $cid);

    }
    else
    {
      if ($outputtype =="json" || $outputtype =="JSON") {
          drupal_json_output ($cached->data);
      }else{

        die($cached->data);
      }
    }
}


/**
 * function to trigger the rebuilding of a stations play list for day 0,1,2,3 and latest
 * uses the class ImportRCSZettaPosts, but doesn't require a post of data
 * @param $station
 */
function ucb_playlist_displayRebuildCache($station)
{
    require_once ("ImportRCSZettaPosts.php");
    $import = new ucbPlaylist\ImportRCSZettaPosts(false, $station);

}