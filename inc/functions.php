<?php
/**
 * Created by PhpStorm.
 * User: matty
 * Date: 15/12/2016
 * Time: 12:51
 */


/* basic playlist functions */



/**
 * takes a station passed in, say from a url, querystring and checks it against the page
 * @param $station
 * @return bool
 */
function aw_playlist_veryifyStation($station)
{
    $stations = aw_playlist_stationsSettings();
    if (isset($stations[$station])) {
        return true;
    }
    return false;
}




/**
 * Build the cache name for a station/day/html or json and if requested, return the cached data itself
 *
 * @param string $output - either output the cid/cache name or the cached data itself
 * @param string $station -  see the key names from aw_playlist_stationsSettings()
 * @param int $day for playlist we can use 0,1,2,3 as the days, latest is forced to be set to 0
 * @param string $type - can be playlist or latest
 * @param bool $json - if set to true the cid name will be altered to return the json array, if false we return the prebuilt html
 * @return bool|string
 */
function aw_playlist_CacheCids($output = "cid", $station = "", $day = 0, $type = "playlist", $json = false, $tries = 0)
{
    $tries++;
    $cache_save_name = false;
    switch ($type) {
        case  "playlist":
            $cache_save_name = "playlistcache_";
            break;
        case "latest":
            $day = 0;
            $cache_save_name = "playlistlatest_";
            break;
    }

    $cache_save_name .= "" . $station . "_" . $day;
    #echo $cache_save_name ;
    if ($json == true) {
        $cache_save_name = "json" . $cache_save_name;
    }
    if ($output == "cache") {
        $cached = cache_get($cache_save_name);
        if ($cached == false) ///looks like the cache has been cleared, lets rebuild it if we haven't tried too many times
        {
            if ($tries > 2) {
                return false;
            } ///ok we've tried to rebuild the cache and it's not happening so return false
            require_once("ImportRCSZettaPosts.php");
            $import = new ucbPlaylist\ImportRCSZettaPosts(false, $station); ///rebuild the cache
            return aw_playlist_CacheCids($output, $station, $day, $type, $json, $tries); ///rebuild the cache if at all possible
            //return false;
        } else {
            return $cached->data;
        }
    }

    return $cache_save_name;
}
