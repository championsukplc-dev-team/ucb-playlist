<?php
/**
 * Playlist admin lets the admin users search through the play listings and remove songs, or simply view songs held in the database
 * User: matty
 * Date: 21/12/2016
 * Time: 16:56
 */

/**
 * function connected to the admin
 * @return string
 */
function ucb_playlist_admin_landingURL()
{
    return "admin/content/playlists/";
}



function ucb_playlist_admin_landingRemove($rowID = 0)
{
    $sql = "DELETE from aw_station WHERE id = " . $rowID;
    db_query($sql);
    echo "REMOVED";
}
